/*
 Analog Input: LDR
 based on  http://arduino.cc/en/Tutorial/AnalogInput

 read sensor-value (LDR) via analog in 0,
 throw value out via serial-port
 and let - depending on the read value - the LED
 on digital pin 13 blink.
 
 * LDR attached to analog input 0
 * LED anode (long leg) attached to digital output 13
 * LED cathode (short leg) attached to ground
 
 * Note: because most Arduinos have a built-in LED attached 
 to pin 13 on the board, the LED is optional.
 
 */

int sensorPin = A0;   // input pin for the sensor
int ledPin = 13;      // pin for the LED
int sensorValue = 0;  // variable to store the value coming from the sensor

void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);

  // setup (debug) output (serial monitor)
  Serial.begin(9600);
}

void loop() {
  // read the value from the sensor:
  sensorValue = analogRead(sensorPin);
  Serial.println(sensorValue);  
  // turn the ledPin on
  digitalWrite(ledPin, HIGH);  
  // stop the program for <sensorValue> milliseconds:
  delay(sensorValue);          
  // turn the ledPin off:        
  digitalWrite(ledPin, LOW);   
  // stop the program for for <sensorValue> milliseconds:
  delay(sensorValue);                  
}
