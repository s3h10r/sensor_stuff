/*

 Analog Input: LDR
 Analog (PWM wave) Output: first LED (represents measured value)
 Digital Output: second LED (represents clock-ticks)
 Data Out: serial
  
 */

const int sensorPin = A0;   // input pin for the sensor
const int ledPin = 9;       // the LED represents measured value
const int ledTick = 13;     // the (onboard) LED represents clock-ticks

const int wait_ms = 1000; // "resolution": get one value any wait_ms 

int sensorValue = 0;  // variable to store the value coming from the sensor

void setup() {
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);

  // setup (debug) output (serial monitor)
  Serial.begin(9600);
}

void loop() {
  sensorValue = analogRead(sensorPin);
  Serial.println(sensorValue);
  // invert sensorValue (dark environment -> bright led)
  // and map sensorvalue to range of 0-255
  analogWrite(ledPin, map(1024 - sensorValue, 0,1023,0,255));

  digitalWrite(ledTick,LOW);
  delay(wait_ms);
  digitalWrite(ledTick,HIGH);
 
}
