sensor_stuff
============

playground... blabla... learning ... misc stuff... fun ... :)

retrieving sensor_data
----------------------

using an arduino

- measure_LDR.ino
- ...

using a raspi/pc connected to the arduino via serial-connection:
(automatically emulated by establishing an usb-connection (/dev/ttyUSB<n>)).

- rec_data.py

processing sensor_data
----------------------

working on the data via: python, pygame, matplotlib, bottle, pil,
processing(.js), ...

- ...

