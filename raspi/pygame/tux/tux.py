#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
discovering basics of pygame

framerate, shapes, animation, text, capturing key-strokes,
color (transparency / alpha), ...
"""


import sys,random

try:
    import pygame
    from pygame.locals import * # this is for the key constants (K_a, K_LEFT, etc...)
 
except ImportError, err:
    print "%s Failed to Load Module: %s" % (__file__, err)
    sys.exit(1)

class Game(object):
    def __init__(self):
        pygame.init()

        self.SIZE  = self.WIDTH, self.HEIGHT = 640, 480
        self.FPS   = 25
        self.clock = pygame.time.Clock()  # create the clock
        self.speed = [2, 2] # moving speed of sprite

        self.screen = pygame.display.set_mode(self.SIZE)

        pygame.display.set_caption("tux.py")

        self.sprite = pygame.image.load("tux2-137x150.png")
        self.spriterect = self.sprite.get_rect()

        # pay attention to certain events only
        # we want to know if the user hits the X on the window, and we
        # want keys so we can close the window with the esc key
        pygame.event.set_allowed([QUIT, KEYDOWN])


    def run(self):
        """Runs the game. Contains the game loop that computes and renders
        each frame."""

        print 'Starting Event Loop'

        running = True

        rgb_b = 0 # one "byte" for using on RGB tuples
        bgcolor = (0,0,0) # background color

        nrFrame = 0 # current frame nr

        poly_points = [] # points of polygon

        while running:

            # you can limit the fps by passing the desired frames per seccond to tick()
            self.clock.tick(self.FPS)

            # handle pygame events -- if user closes game, stop running
            running = self.handleEvents()
            bgcolor = (0,0,rgb_b)
            self.screen.fill(bgcolor) # set background color

            # update the title bar with our frames per second
            pygame.display.set_caption('tux.py - %d fps' % self.clock.get_fps())

            # draw rectangle(s)
            pygame.draw.rect(self.screen, (rgb_b,0,0), Rect((100,100),(50,25)))
            pygame.draw.rect(self.screen, (0,rgb_b,0), Rect((100,200),(50,25)))
            pygame.draw.rect(self.screen, (0,0,255-rgb_b), Rect((100,300),(50,25)))

            # draw circle
            if rgb_b % 8 > 0:
                pygame.draw.circle(self.screen, (0,rgb_b,255-rgb_b), (self.WIDTH / 2,self.HEIGHT /2), self.WIDTH / (rgb_b % 8))


            # draw polygon
            if nrFrame % (self.FPS * 2) == 0 or nrFrame == 0: # create a new shape?
                poly_points = []             
                for i in range(random.randint(3,16)): # 3 - 16 points
                    poly_points.append([random.randint(0,self.WIDTH),random.randint(0,self.HEIGHT)])

            # doing alpha-stuff (RGBA): create a second surface and then blit it to the screen.
            # Blitting will do alpha blending and color keys. 

            # create a fully transparent surface in the size of the screen(-surface)
            # to make sure we dunnot "overdraw" the screen(-surface)
            s = pygame.Surface((self.WIDTH,self.HEIGHT), pygame.SRCALPHA)   # per-pixel alpha
            s.fill((0,0,0,0))                   # fully transparent surface (alpha=0)
            # throw our polygon with alpha=180 on this surface
            pygame.draw.polygon(s,(255,255,0,140),poly_points)
            self.screen.blit(s, (0,0))

            # draw sprite
            self.spriterect = self.spriterect.move(self.speed)
            if self.spriterect.left < 0 or self.spriterect.right > self.WIDTH:
                self.speed[0] = -self.speed[0]
            if self.spriterect.top < 0 or self.spriterect.bottom > self.HEIGHT:
              self.speed[1] = -self.speed[1]

          
            #self.screen.blit(self.sprite, (self.WIDTH / 2, self.HEIGHT / 2)) # one sprite "still"

            self.screen.blit(self.sprite, self.spriterect) # moving sprite

            # draw some text
            font=pygame.font.Font(None,30)
            #font=pygame.font.Font("monospace",15)
            text=font.render("fps: "+ str(int(self.clock.get_fps())) + " rgb_b:"+str(rgb_b), 1,(255,255,255))
            text=font.render("fps: %d rgb_b: %s frame: %d" % (int(self.clock.get_fps()), rgb_b, nrFrame), 1,(255,255,255))
            self.screen.blit(text, (100, 450))


            # render the screen
            pygame.display.flip()

            # increase/decrease blue-value
            if rgb_b == 255:
                rgb_b1 = -1
            elif rgb_b == 0:
                rgb_b1 = 1
            rgb_b += rgb_b1

            nrFrame += 1

        print 'Bye.'


    def handleEvents(self):
        """Poll for PyGame events and behave accordingly. Return false to stop
        the event loop and end the game."""

        # poll for pygame events
        for event in pygame.event.get():
            if event.type == QUIT:
                return False

            # handle user input
            elif event.type == KEYDOWN:
                # if the user presses escape, quit the event loop.
                if event.key == K_ESCAPE:
                    return False
        return True

# create a game and run it
if __name__ == '__main__':
    game = Game()
    game.run()
