#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
plotter for series of measurement in csv-format.
can be used to plot data of tools like sar etc.

usage: %prog csv-file [options]
   -k, --key = string : column-name in given csv file to use as "primary-key"
   -f, --field2start = integer : column-number where values to plot start
   -s, --size = string : resolution of generated picture, e.g. 2000x1600
   -o, --output = string : filename for output 
   -c, --console: do not use X-Server / don't show plots O
   -e, --element = string: one ore more values matching primary key (default: all) 
   -n, --name = string: plot's name 
   -v, --version: show version 


example:

    take a look at the included `example.sh` & "example*.csv".

adcanced examples by using sar data:

    sadf -d /var/log/sa/sa21 -- -d > /tmp/sar.disk.21.csv
    ./idoplot.py /tmp/sar.disk.21.csv -k DEV

    or

    sadf -d /var/log/sa/sa21 -- > /tmp/sar.cpu.21.csv
    ./idoplot.py /tmp/sar.cpu.21.csv -k CPU -s 2000x1600 -o /tmp/myplot.png --console

    or

    sadf -d /var/log/sa/sa21 -- -n DEV > /tmp/sar.net.21.csv
    ./idoplot.py /tmp/sar.net.21.csv -k IFACE 

    or (solaris workaround)

    sar -b | ./sarsol2csv.py - > sar.sol.bufferactivity.csv
    ./idoplot.py ./sar.sol.bufferactivity.csv -k "# hostname" -f 2   

    or 

    sar -d | ./sarsol2csv.py - > sar.sol.disk.csv
    ./idoplot.py ./sar.sol.disk.csv -k "device" -e st11,st12 -f 3 
"""

import sys,re
import dateutil.parser

### matplotlib stuff

import numpy as np
import matplotlib as mpl
##mpl.use("Agg") # agg-backend, so we can create figures without x-server (no PDF, just PNG etc.)
#mpl.use("Cairo") # cairo-backend, so we can create figures without x-server (PDF & PNG ...)
#import matplotlib.pyplot as plt

### logging
import logging,logging.config
### simple-logging config without conf-file
#LOGFILE="/tmp/bla.log"
#logging.basicConfig(filename=LOGFILE,level=logging.DEBUG,format= "%(asctime)s - %(name)s - [%(levelname)-8s] %(message)s")

logging.basicConfig(level=logging.DEBUG,format= "%(asctime)s - %(name)s - [%(levelname)-8s] %(message)s")
logger = logging.getLogger("idoplot")


__author__  = 'Sven Hessenmueller (sven.hessenmueller@gmail.com)'
__version__ = '0.1.12-120918'
__date__ = "20110129"
__copyright__ = "Copyright (c) 2011 %s" % __author__
__license__ = "GPL"


def parseCSV(fname,delimiter=';'):
    """
    almitra2:/home/hessenm/perf_monitoring # sadf -d /var/log/sa/sa21 -- -d | head -4
    # hostname;interval;timestamp;DEV;tps;rd_sec/s;wr_sec/s;avgrq-sz;avgqu-sz;await;svctm;%util
    almitra2;1201;2011-01-20 23:20:01 UTC;dev104-0;1.84;0.00;28.10;15.31;0.08;43.68;5.44;1.00
    almitra2;1201;2011-01-20 23:20:01 UTC;dev9-0;0.00;0.00;0.00;0.00;0.00;0.00;0.00;0.00
    almitra2;1201;2011-01-20 23:20:01 UTC;dev9-10;0.00;0.00;0.00;0.00;0.00;0.00;0.00;0.00
    """
    import csv

    dicts = []

    f = open(fname,'rU')
    parser = csv.reader(f, delimiter=delimiter)
    firstRec = True
    lstFieldNames = None

    for fields in parser:
        if firstRec or not lstFieldNames:
            if fields[0].strip()[0] == "#":
                lstFieldNames = fields
                lstFieldNames[0] = lstFieldNames[0].replace('#','').strip() # dirty bugfix
            else:
                logger.warning("header not in first line? (no '#' hook?) - skipping: %s" % fields)
            firstRec = False
        else:
            if len(fields) != len(lstFieldNames):
                logger.warning("length of fields seems wrong - skipping: %s" % fields)
            else:
                dicts.append({})
                for i,f in enumerate(fields):
                    dicts[-1][lstFieldNames[i]] = f


    return lstFieldNames, dicts


def procData(sFNames, lDctCSV, sPrimkey = "DEV", sTSkey = "timestamp"):
    """
    process Data leeched out of CSV file via parseCSV
    (e.g. convert datestrings into datetime objects)

    returns
         dictData[<valPrimkey>][<fieldname>] = [ <date>, <val> ]

         e.g. dictData['dm-14']['tps'] = [ <date>, <val> ]
    """

#    assert lstFieldNames == ['# hostname', 'interval', 'timestamp', 'DEV', 'tps', 'rd_sec/s', 'wr_sec/s', 'avgrq-sz', 'avgqu-sz', 'await', 'svctm', '%util'], 'Woups! Sorry, csv file is not in right format?'

    lNames, lDates, lValues = [], [], []

    lNames = sFNames
    dData = {}

    for dct in lDctCSV:
        primkey = "%s" % (dct[sPrimkey])
        if not dData.has_key(primkey):
            dData[primkey] = {}
            for l in lNames:
                dData[primkey][l] = []  # e.g. dData['dm-14']['tps'] = [ <date>, <val> ]

        for l in lNames:
            dData[primkey][l].append([ dateutil.parser.parse(dct[sTSkey]), dct[l] ])
 
        


    return dData


def trendgraph_date(lstNames,lstDates,lstValues,strTitle = "Title",strYLabel="GiB",lstColor=['#0000FF','#00FF00','#FF0000','#000000'],marker=""):
    """
    """
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
    plt.title(strTitle)
    plt.ylabel(strYLabel)

    for i,name in enumerate(lstNames):
        #ax1.plot_date(lstDates[i], lstValues[i],linestyle='-')
        #ax1.plot_date(lstDates[i], lstValues[i],linestyle='-'label=name,color=lstColor[i % len(lstColor)])
        ax1.plot_date(lstDates[i], lstValues[i],linestyle='-',linewidth=2.5,marker=marker,label=name,color=lstColor[i % len(lstColor)])

#        print "#", name
#        for i2,el in enumerate(lstDates[i]):
#            print el,lstValues[i][i2]

    #monthsLoc = mpl.dates.MonthLocator()
    #weeksLoc = mpl.dates.WeekdayLocator()
    #ax1.xaxis.set_major_locator(monthsLoc)
    #ax1.xaxis.set_minor_locator(weeksLoc)
    #monthsFmt = mpl.dates.DateFormatter('%b %Y')
    #ax1.xaxis.set_major_formatter(monthsFmt)
    fig.autofmt_xdate(bottom=0.18) # adjust for date labels display

    #plt.legend()
    plt.legend(loc="best") # locate the legend in the best way :-)
    plt.grid(True)
    plt.show()



def trendgraph_date_addsubplot(fig, subplotparm , lstNames,lstDates,lstValues,strTitle = "Title",strYLabel="GiB",lstColor=['#0000FF','#00FF00','#FF0000','#000000'],marker=""):
    ax1 = fig.add_subplot(subplotparm[0],subplotparm[1],subplotparm[2])
    plt.title(strTitle)
    plt.ylabel(strYLabel)

    for i,name in enumerate(lstNames):
        #ax1.plot_date(lstDates[i], lstValues[i],linestyle='-')
        #ax1.plot_date(lstDates[i], lstValues[i],linestyle='-'label=name,color=lstColor[i % len(lstColor)])
        ax1.plot_date(lstDates[i], lstValues[i],linestyle='-',linewidth=2.5,marker=marker,label=name,color=lstColor[i % len(lstColor)])

#        print "#", name
#        for i2,el in enumerate(lstDates[i]):
#            print el,lstValues[i][i2]

    #monthsLoc = mpl.dates.MonthLocator()
    #weeksLoc = mpl.dates.WeekdayLocator()
    #ax1.xaxis.set_major_locator(monthsLoc)
    #ax1.xaxis.set_minor_locator(weeksLoc)
    #monthsFmt = mpl.dates.DateFormatter('%b %Y')
    #ax1.xaxis.set_major_formatter(monthsFmt)
    fig.autofmt_xdate(bottom=0.18) # adjust for date labels display

    plt.legend(loc="best") # locate the legend in the best way :-)
    plt.grid(True)



def usecase01(sHost, dData):
    """
    one datafield of primkey (e.g. device) in diagram
    """
    lNames, lDates, lValues = [],[],[]

    for k in sorted(dData.keys()):

        lDat, lVal = [],[]
        for m in dData[k]:
            lNames.append(k)
            lDat = []; lVal = []

            for lst in dData[k][m]:
                lDat.append(lst[0]); lVal.append(lst[1])

            lDates.append(lDat); lValues.append(lVal)
            trendgraph_date(lNames,lDates,lValues,"SAR data %s %s %s" % (sHost,k,m), strYLabel = "")
            lNames, lDates, lValues = [],[],[]

def usecase01_2(sHost, dData):
    """
    one datafield of all primkeys (e.g. device) in diagram
    """

    lNames, lDates, lValues = [],[],[]

    fields = ['tps','%util']

    for f in fields:
        for k in sorted(dData.keys())[:3]: # just for 3 keys
            lNames.append(k)

            lDat, lVal = [],[]
            for lst in dData[k][f]:

                lDat.append(lst[0]); lVal.append(lst[1])

            lDates.append(lDat); lValues.append(lVal)
        trendgraph_date(lNames,lDates,lValues,"SAR data %s %s %s" % (sHost,k,f), strYLabel = "")
        lNames, lDates, lValues = [],[],[]


def usecase02(sPlotname, dData, size = (20,16), dpi = 100, lElements = None, outfile="/tmp/iplot_out.png"):
    """
    all (or those matching lElements) data fields of primkey (e.g. device)
    in one big diagram (via subplots)
    """    

    lNames, lDates, lValues = [],[],[]

    if not lElements:
        lDataKeys = sorted(dData.keys()) 
    else:
        lDataKeys = []
        for e in lElements:
            if e in dData.keys(): lDataKeys.append(e)

    for k in lDataKeys:
        fig = plt.figure(figsize=size,dpi=dpi) # 2000x1600

        lDat, lVal = [],[]
        nrFields = len(dData[k])

        for i,m in enumerate(dData[k]):
            lNames.append(m)
            lDat = []; lVal = []

            for lst in dData[k][m]:
                lDat.append(lst[0]); lVal.append(lst[1])

            lDates.append(lDat); lValues.append(lVal)
            if i == 0: # first plot?
                trendgraph_date_addsubplot(fig, [nrFields,1,i+1], lNames,lDates,lValues,"all data-fields of %s (%s)" % (sPlotname,k), strYLabel = "")
            else:
                trendgraph_date_addsubplot(fig, [nrFields,1,i+1], lNames,lDates,lValues,"", strYLabel = "")


            lNames, lDates, lValues = [],[],[]


        plt.savefig(outfile)
        plt.show()


#### nice inline option parsing

#source: http://code.activestate.com/recipes/278844/
#by M. Simionato

import optparse, re, sys

USAGE = re.compile(r'(?s)\s*usage: (.*?)(\n[ \t]*\n|$)')

def nonzero(self): # will become the nonzero method of optparse.Values
    "True if options were given"
    for v in self.__dict__.itervalues():
        if v is not None: return True
    return False

optparse.Values.__nonzero__ = nonzero # dynamically fix optparse.Values

class ParsingError(Exception): pass

optionstring=""

def exit(msg=""):
    raise SystemExit(msg or optionstring.replace("%prog",sys.argv[0]))

def parse(docstring, arglist=None):
    global optionstring
    optionstring = docstring
    match = USAGE.search(optionstring)
    if not match: raise ParsingError("Cannot find the option string")
    optlines = match.group(1).splitlines()
    try:
        p = optparse.OptionParser(optlines[0])
        for line in optlines[1:]:
            opt, help=line.split(':')[:2]
            short,long=opt.split(',')[:2]
            if '=' in opt:
                action='store'
                long=long.split('=')[0]
            else:
                action='store_true'
            p.add_option(short.strip(),long.strip(),
                         action = action, help = help.strip())
    except (IndexError,ValueError):
        raise ParsingError("Cannot parse the option string correctly")
    return p.parse_args(arglist)


#### END option parsing

if __name__ == "__main__":

    option,args=parse(__doc__)
    if not args and not option: exit()
    elif option.version:
        print __version__
        print __copyright__
        sys.exit()

    elif len(args) != 1: print "...mmh... please GIVE ME A CSV FILE to eat..."; exit()


    fname = args[0]

    if option.console: # no xserver? .
        ##mpl.use("Agg") # agg-backend, so we can create figures without x-server (no PDF, just PNG etc.)
        mpl.use("Cairo") # cairo-backend, so we can create figures without x-server (PDF & PNG ...)

    import matplotlib.pyplot as plt # we have to import the pyplot object AFTER setting backend

        
    if option.key:
        sPrimkey = option.key
    else:
        sPrimkey = "DEV"

    if option.field2start:
        iField2start = int(option.field2start)
    else:
        iField2start = 4

    if option.output:
        sOutfile = option.output
    else:
        sOutfile = "/tmp/iplot_out.png"

    iDPI = 100
    if option.size:
       tSize = tuple([ int(s)/iDPI for s in option.size.split("x") ])
       print tSize
    else:
        tSize = (2000/iDPI, 1600/iDPI)

    if option.element:
        lElements = option.element.split(',')
    else:
        lElements = None

    sPlotname = "<plotname>"
    if option.name:
        sPlotname = option.name
     
    lstFieldNames, lstDctCSV = parseCSV(fname)
    lstFieldNames = lstFieldNames[iField2start:]# kick '# hostname', 'interval', 'timestamp'
    
    dData = procData(lstFieldNames,lstDctCSV,sPrimkey)

    #usecase01(sHost,dData) 
    #usecase01_2(sHost,dData) 
    usecase02(sPlotname,dData,tSize, lElements = lElements, outfile = sOutfile) 

            

