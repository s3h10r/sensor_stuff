#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
read val from tty (arduino) & write into sqlite-database table 'data_raw'

example usage: 

    run this on a raspi while a connected arduino is running "measure_LDR.ino"
"""

import sys,serial
from sqlite3 import dbapi2 as sqlite

TTY='/dev/ttyUSB0' # device-node of our Arduino
TTY_BAUD=9600

DB_FILE='rec_data.sqlite3'
#DB_FILE=':memory:' # in-memory 

def getTimestamp(fmt="%Y-%m-%d %H:%M:%S"):
    import datetime
    return datetime.datetime.now().strftime(fmt)

def readSerial(ser_con):
    """
    read one line from serial and return it
    """
    line = ser_con.readline()
    line = line.strip()
    return line
    

if __name__ == '__main__':

    # setup connection
    ser_con = serial.Serial(TTY, TTY_BAUD)

    # setup database
    db = sqlite.connect(DB_FILE)
    dbc = db.cursor()
    initSQL = '''
            CREATE TABLE IF NOT EXISTS "data_raw" (
            ts       varchar(80),
            line     varchar(80),
            UNIQUE ("ts")
        );
    '''
    dbc.execute(initSQL)
    db.commit()

    # here we go
    while True:
        line = readSerial(ser_con)
        ts = getTimestamp()
        print >> sys.stderr, "%s;%s" % (ts,line)

        dbc.execute("insert into 'data_raw' VALUES ('%s','%s')" % (ts, line))
        db.commit() # TODO: commit each 5min or so instead after every read

