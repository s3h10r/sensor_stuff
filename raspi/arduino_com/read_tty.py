#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
reads the serial output (e.g. sensor-data) of an arduino
plugged into usb-port
"""

TTY='/dev/ttyUSB0' # device-node of our Arduino
TTY_BAUD=9600

import serial

ser = serial.Serial(TTY, TTY_BAUD)
while True:
    print ser.readline()

