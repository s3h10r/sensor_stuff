#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
mapping a range of values (e.g. sensor-data) to another (e.g. for an actuator)

stackoverflow.com/questions/1969240/mapping-a-range-of-values-to-another
"""

def make_interpolater(left_min, left_max, right_min, right_max, check_boundary=True):
    """
    by using this function you can write your processor as:
 
    # create function for doing interpolation of the desired
    # ranges 
    scaler = make_interpolater(1, 512, 5, 10)

    # receive list of raw values from sensor, assign to data_list

    # now convert to scaled values using map 
    scaled_data = map(scaler, data_list)

    # or a list comprehension, if you prefer
    scaled_data = [scaler(x) for x in data_list]
    """

    # Figure out how 'wide' each range is  
    leftSpan = left_max - left_min  
    rightSpan = right_max - right_min  

    # Compute the scale factor between left and right values 
    scaleFactor = float(rightSpan) / float(leftSpan) 

    # create interpolation function using pre-calculated scaleFactor
    def interp_fn(value):
        if check_boundary:
            if value > left_max: 
                raise Exception, ("value %d > left_max %d" % (value, left_max))
            elif value < left_min: 
                raise Exception, ("value %d < left_min %d" % (value, left_min))
        
        
        return right_min + (value-left_min)*scaleFactor

    return interp_fn

def __selftest():
    """
    mapping range(0,1024) -> range(0,255)"
    """

    #sensor_values = [0, 200, 400, 600, 800, 1000, 1023, 1200] # should fail if check_boundary
    #sensor_values = [-10, 200, 400, 600, 800, 1000, 1023, 1200] # should fail if check_boundary
    sensor_values = [0, 200, 400, 600, 800, 1000, 1023] # should work if check_boundary

    scaler = make_interpolater(0, 1023, 0, 255)
    actuator_values = map(scaler, sensor_values) # scaled data

    print "_selftest() : "
    print "...mapping range(0,1024) -> range(0,255)"
    print "IN sensor_values : %s" % sensor_values       
    print "OUT actuator_values : %s" % actuator_values       

if __name__ == '__main__':
    __selftest()

